# Savage Helper

A Savage Worlds GM helper tool designed for Windows. Built with C++ using the Qt Framework. 

## To Build

* Add Build Info

## License Info

Savage Helper is shared under the MIT License. See license.txt for more details.

This software references the Savage Worlds game system, available from Pinnacle Entertainment Group at www.peginc.com. Savage Worlds and all associated logos and trademarks are copyrights of Pinnacle Entertainment Group. Used with permission. Pinnacle makes no representation or warranty as to the quality, viability, or suitability for purpose of this product.